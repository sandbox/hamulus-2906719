INTRODUCTION
------------
The Contact Block module Adds configurable suffix and prefix for contact blocks, 
possibility to rename submit button and disable preview button

REQUIREMENTS
------------
This module requires the following modules:

* Contact Block module

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/documentation/install/modules-themes/modules-8
  for further information.

CONFIGURATION
-------------
All configuration options appear at configuration forms of contact blocks.
