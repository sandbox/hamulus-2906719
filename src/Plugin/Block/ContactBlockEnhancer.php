<?php

namespace Drupal\contact_block_enhancer\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\contact_block\Plugin\Block\ContactBlock;

/**
 * Provides a 'ContactBlock' block.
 *
 * @Block(
 *  id = "contact_block",
 *  admin_label = @Translation("Contact block"),
 * )
 */
class ContactBlockEnhancer extends ContactBlock implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $form['prefix'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('HTML prefix'),
      '#default_value' => $this->configuration['prefix'],
    ];

    $form['suffix'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('HTML suffix'),
      '#default_value' => $this->configuration['suffix'],
    ];

    $form['button_text'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Submit button text'),
      '#default_value' => $this->configuration['button_text'],
    ];

    $form['disable_preview'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Disable Preview button'),
      '#default_value' => $this->configuration['disable_preview'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $this->configuration['prefix'] = $form_state->getValue('prefix');
    $this->configuration['suffix'] = $form_state->getValue('suffix');
    $this->configuration['button_text'] = $form_state->getValue('button_text');
    $this->configuration['disable_preview'] = $form_state->getValue('disable_preview');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = parent::build();

    if (!empty($this->configuration['prefix'])) {
      $form['#prefix'] = $this->configuration['prefix'];
    }
    if (!empty($this->configuration['suffix'])) {
      $form['#suffix'] = $this->configuration['suffix'];
    }

    if (!empty($this->configuration['disable_preview'])) {
      unset($form['actions']['preview']);
    }

    if (!empty($this->configuration['button_text'])) {
      $form['actions']['submit']['#value'] = $this->configuration['button_text'];
    }

    return $form;
  }

}
